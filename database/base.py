from operation_settings.settings import DB_USER, DB_PASSWORD, DB_HOST, DB_NAME

__author__ = 'william'

import sqlalchemy
from sqlalchemy.ext import declarative
from sqlalchemy.orm import sessionmaker, scoped_session

"""'
 Use this format
mysql://root:@localhost/cipher_transport'
'mysql://%s:%s@%s/%s' % (DB_USER, DB_PASSWORD, DB_HOST, DB_NAME),
"""
engine = sqlalchemy.create_engine(
    'sqlite:///:memory:',
    connect_args={
        "check_same_thread": False
    }
)

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
ModelBase = declarative.declarative_base()


def setup_db():
    ModelBase.metadata.create_all(bind=engine)