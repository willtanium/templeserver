import datetime
import os
import sqlalchemy
from sqlalchemy.orm import relation
from database import base

__author__ = 'william'


class UserAccount(base.ModelBase):
    __tablename__ = "Users"
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    friend_key = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    first_name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    last_name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    photo = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    email_address = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    phone_number = sqlalchemy.Column(sqlalchemy.String, nullable=False)


class GroupAccount(base.ModelBase):
    __tablename__ = "Groups"
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    group_key = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    group_name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    group_description = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    group_photo = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    creator_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('Users.id'))
    creator = relation("UserAccount", backref='groups_created', lazy=False)


class InvitationDB(base.ModelBase):
    __tablename__ = "Invitations"
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    group_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('Groups.id'))
    personal_message = sqlalchemy.Column(sqlalchemy.String)
    group = relation("GroupAccount", backref="group", lazy=False)


class SharedFile(base.ModelBase):
    __tablename__ = "SharedFiles"
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    file_path = sqlalchemy.Column(sqlalchemy.String)
    file_extension = sqlalchemy.Column(sqlalchemy.String)
    file_key = sqlalchemy.Column(sqlalchemy.String)
    date_uploaded = sqlalchemy.Column(sqlalchemy.DateTime, default=datetime.datetime.utcnow)

    @property
    def file_size(self):
        size = os.path.getsize(self.file_path)
        return size
