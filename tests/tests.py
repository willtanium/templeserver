import unittest
import uuid
from database import base
from database.models.db_models import UserAccount, GroupAccount, InvitationDB, SharedFile


__author__ = 'william'


class DatabaseTests(unittest.TestCase):
    def setUp(self):
        self.test_user = UserAccount(first_name='william',
                                     last_name='kibira',
                                     photo='/images/fake_photo.jpg',
                                     email_address='williamkibira@gmail.com',
                                     friend_key='error_key',
                                     phone_number='+256791149189')
        self.test_group = GroupAccount(creator=self.test_user,
                                       group_name='Woloks',
                                       group_key='ttext',
                                       group_description='Just a test',
                                       group_photo='/images/fake_gp_photo.jpg',
                                       )
        self.test_invitation = InvitationDB(group=self.test_group,
                                            personal_message="You test went well")

        self.test_file = SharedFile(file_path="/shared_files/oshiro.mp4",
                                    file_extension=".mp4",
                                    file_key='2223')
        base.setup_db()

    def tearDown(self):
        base.Session.close()

    def test_saving_user(self):

            base.Session.add(self.test_user)
            base.Session.flush()
            ret_user = base.Session.query(UserAccount).filter_by(friend_key=self.test_user.friend_key).first()
            self.assertEquals(self.test_user.first_name, ret_user.first_name, msg='Returned User is a match')

    def test_saving_group(self):
            base.Session.add(self.test_user)
            base.Session.add(self.test_group)
            base.Session.flush()
            ret_group = base.Session.query(GroupAccount).filter_by(group_key=self.test_group.group_key).first()
            print("Group Creator %s" % ret_group.creator.first_name)
            self.assertEquals(self.test_user.id, ret_group.creator.id)

    def test_saving_invitation(self):
        base.Session.add(self.test_user)
        base.Session.add(self.test_group)
        base.Session.add(self.test_invitation)
        base.Session.flush()
        ref_invitation = base.Session.query(InvitationDB).first()
        print("Invitation %s" % ref_invitation.personal_message)
        self.assertEquals(self.test_group.id, ref_invitation.group.id)

    def test_shared_file(self):
        base.Session.add(self.test_file)
        base.Session.flush()
        print("File location was persisted")
        file_data = base.Session.query(SharedFile).filter_by(file_key='2223').first()
        self.assertEquals(self.test_file.file_path, file_data.file_path)



