import falcon
from database import base
from database.models.db_models import UserAccount
from protobuffer.temple_pb2 import Friend, SearchResponse, SearchQuery

__author__ = 'william'


class FriendsSearch(object):
    def on_post(self, req, resp):
        search_query = SearchQuery()
        search_query.ParseFromString(req.stream.read())
        response = self.__get_relates_friends(search_query.phone_numbers)
        resp.body = response.SerializeToString()
        resp.status = falcon.HTTP_200

    def __get_relates_friends(self, contact_list):
        search_response = SearchResponse()
        for contact in contact_list:
            friend = search_response.friends.add()
            friend.CopyFrom(self.__get_friend_contact(contact))
        return search_response

    def __get_friend_contact(self, contact):
        friendDB = base.Session.query(UserAccount).filter_by(phone_number=contact).first()
        friend = Friend()
        friend.first_name = friendDB.first_name
        friend.last_name = friendDB.last_name
        friend.photo_data = self.read_photo_data(friendDB.photo)
        friend.email_address = friendDB.email_address
        friend.phone_number = friendDB.phone_number
        return friend

    def read_photo_data(self, file_name):
        with open(file_name, 'wb') as data_file:
            return data_file.read()
