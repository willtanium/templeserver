import os
import uuid
import falcon
from database import base
from database.models.db_models import UserAccount, GroupAccount
from operation_settings.settings import PROFILE_DIRECTORY
from protobuffer.temple_pb2 import UserReg, UserData, Group

__author__ = 'william'


def save_image(photo_data, friend_key):
    file_name = os.path.join(PROFILE_DIRECTORY, friend_key + ".png")
    with open(file_name, 'wb') as data_file:
        data_file.write(photo_data)
        data_file.close()


class FriendRegistration(object):
    def on_post(self, req, resp):
        friend_key = str(uuid.uuid4())

        user_reg = UserReg()
        user_reg.ParseFromString(req.stream.read())
        file_name = save_image(user_reg.photo_data, friend_key)
        user_db = UserAccount(friend_key=friend_key,
                              first_name=user_reg.first_name,
                              last_name=user_reg.last_name,
                              photo=file_name,
                              email_address=user_reg.email_address,
                              phone_number=user_reg.phone_number)
        base.Session.add(user_db)
        base.Session.flush()
        user_resp = UserData()
        user_resp.friend_key = friend_key
        user_resp.account_folder = friend_key
        resp.body = user_resp.SerializeToString()
        resp.status = falcon.HTTP_200


class GroupRegistration(object):
    def on_post(self, req, resp):
        group_key = str(uuid.uuid4())

        group_reg = Group()

        group_reg.ParseFromString(req.stream.read())
        file_name = save_image(group_reg.group_logo, group_key)
        creator = base.Session.query(UserAccount).filter_by(friend_key=group_key.creator_key).first()
        group_db = GroupAccount(group_name=group_reg.group_name,
                                group_key=group_key,
                                group_description=group_reg.group_description,
                                group_photo=file_name,
                                creator=creator)
        base.Session.add(group_db)
        base.Session.flush()
        resp.body = ("Group %s was registered" % group_reg.group_name)
        resp.status = falcon.HTTP_200