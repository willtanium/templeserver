import falcon
from database import base
from database.models.db_models import UserAccount, GroupAccount
import pika
from operation_settings.settings import MESSAGE_QUEUE_HOST
from protobuffer.temple_pb2 import Invitation

__author__ = 'william'


class Invitations(object):
    def __init__(self):
        self.connection = None
        self.channel = None

    def on_post(self, req, resp):
        invitation = Invitation()
        invitation.ParseFromString(req.stream.read())
        group = base.Session.query(GroupAccount).filter_by(group_key=invitation.group_key).first()
        invitationDB = Invitations(group=group, personal_message=invitation.personal_message)
        base.Session.add(invitationDB)
        base.Session.flush()
        self.__invitation_extractor(invitation.contacts, invitation.personal_message)
        resp.body = "Invitations have been sent out"
        resp.status = falcon.HTTP_200

    def __invitation_extractor(self, contact_list, personal_message):
        self.__set_invitation_handler()
        for contact in contact_list:
            user = self.__check_for_user(contact)
            if user == None:
                continue
            self.__send_message(user.friend_key, personal_message)
            print("The user %s %s has been sent an invitation" % (user.first_name, user.last_name))
        self.__close_messenger()

    def __check_for_user(self, phone_number):
        user = base.Session.query(UserAccount).filter_by(phone_number=phone_number).first()
        return user

    def __set_invitation_handler(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=MESSAGE_QUEUE_HOST))
        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange='invitation', type='direct')

    def __send_message(self, routing_key, message):
        self.channel.basic_publish(exchange='invitation', routing_key=routing_key, body=message)

    def __close_messenger(self):
        self.connection.close()


