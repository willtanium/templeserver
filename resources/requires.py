import falcon

__author__ = 'william'


class RequireProto(object):
    def process_request(self, req, resp):
        if req.method in ('POST', 'PUT'):
            if 'application/x-protobuf' not in req.content_type:
                raise falcon.HTTPUnsupportedMediaType(
                    'This API only supports requests encoded as Protobuf.',
                    href='http://google.protobuf/documentation')


class AuthMiddleware(object):
    def process_request(self, req, resp):
        token = req.get_header('X-Auth-Token')
        secret = req.get_header('X-secret')

        if token is None:
            description = ('Please provide an auth token'
                           'as part of your request')

            raise falcon.HTTPUnauthorized('Auth token required',
                                          description=description,
                                          href='http://docs.example.com/auth')
        if not self.__is_token_valid(token, secret):
            description = ('Please provide a valid auth token'
                           'as part of your request')
            raise falcon.HTTPUnauthorized('Authentication required',
                                          description=description,
                                          href='http://docs.example.com/auth')


    def __is_token_valid(self, token, secret):
        return True
