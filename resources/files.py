import os
import uuid
import falcon
from database import base
from database.models.db_models import SharedFile
from protobuffer.temple_pb2 import Attachment

__author__ = 'william'


class FileUpload(object):
    def __init__(self, storage_path):
        self.storage_path = storage_path

    def on_post(self, req, resp):
        file_key = str(uuid.uuid4())
        file_path = os.path.join(self.storage_path, file_key)
        attachment = Attachment()
        with open(file_path, 'wb') as data_file:
            while True:
                chunk = req.stream.read(1024)
                if not chunk:
                    break
                data_file.write(chunk)
            shared_file = SharedFile(file_path=file_path,
                                     file_extension='data',
                                     file_key=file_key)
            base.Session.add(shared_file)
            base.Session.flush()

            attachment.attachment_key = file_key
            attachment.file_size = os.path.getsize(file_path)
        resp.body = attachment.SerializeToString()
        resp.status = falcon.HTTP_201


class FileRequest(object):
    def __init__(self, storage_path):
        self.storage_path = storage_path

    def on_get(self, req, resp, file_key):
        file_data = base.Session.query(SharedFile).query(file_key=file_key).first()
        resp.stream_len = file_data.file_size
        resp.stream = open(file_data.file_path, 'rb')


